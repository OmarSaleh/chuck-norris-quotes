import React, { useState, useEffect } from 'react';

const API_URL = 'https://api.chucknorris.io/jokes/random';

function App() {
    const [joke, setJokes] = useState(' ');

    const generateQuote = () => {
   
        fetch(API_URL)
            .then((resp) => resp.json())
            .then((json) => displayJokes(json))
            .catch((err) => console.log(err));
    }

    useEffect(() => {
      generateQuote();
    }, [])

    const displayJokes = (data) => {
        setJokes(data.value);
    };

    return (
        <div className="box">
            <h3>Chuck Norris - Quotes</h3>
            <p dangerouslySetInnerHTML={{ __html: joke }} />
           
            <button onClick={generateQuote}> Plus de 10 quotes derrière ce boutton</button>
        </div>
    );
}

export default App;